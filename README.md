This repo contains the code for tag/label prediction for stack exchange

## Requirements
Use the following command to install all the required packages
`pip install -r requirements.txt`

### How to Use
- Use `predict.py` in `scripts` folder to get the label prediction (Model files are not included)
```
python predict.py input.txt output.txt
```
